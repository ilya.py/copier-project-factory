# Copier project factory

### System requirements
- Python 3.10+
- [Poetry](https://python-poetry.org/docs/#installing-with-the-official-installer)


### Installation

1. Clone the project and change directory to it:

```shell
git clone git@gitlab.com:ilya.py/copier-project-factory.git && cd copier-project-factory
```

2. Install dependencies:

```shell
poetry install
```

### Usage

**Install a new Django project:**

1. Create project from template:
```shell
copier copy templates/django_template path/to/django_project
```

2. Go to the new project directory:

```shell
cd path/to/django_project
```

3. Install dependencies:

```shell
poetry install
```

4. Copy the `.env.template` with the name `.env`  file and fill it in:

```shell
cp config/.env.template config/.env
nano config/.env
```

5. Run the docker compose:

```shell
docker compose up -d
```

